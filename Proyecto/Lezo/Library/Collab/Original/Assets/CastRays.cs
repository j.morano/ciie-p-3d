﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.Characters.ThirdPerson;

public class CastRays : MonoBehaviour
{
    // Start is called before the first frame update
    private GameObject target;
    private GameObject target_m;
    private GameObject vigilante;
    private GameObject visor_v;
    private int counts;
    private AudioSource audioSourceEmptyObject;
    public AudioClip sonidoTick;
    public AudioClip sonidoAlarm;
    public AudioClip sonidoEnfado;
    public int speed;
    private bool performAction;

    //==========================================================================================
    float time    = 0.0f;
    float angulo_global_x = 0.0f;
    float angulo_parcial_x = 0.0f;
    float angulo_global_y = 0.0f;
    float angulo_parcial_y = 0.0f;
    private bool objetivo_detectado = false;
    private bool en_vigilancia = false;
    float margen = 0.0f;
    Vector3 targetDir;
    float angle;

    Collider m_collider;
    //==========================================================================================

    private GameObject[] enemiesArray;

    //Manage stealth bar
    public GameObject stealthBarUI;
    public Slider slider;

    void Start()
    {
        counts = 0;
        target = GameObject.FindGameObjectWithTag("MainPlayer"); //ThirdPerson controller
        vigilante = transform.parent.gameObject; //("vigilante").gameObject;
        audioSourceEmptyObject = target.transform.Find("SonidoTick").gameObject.GetComponent<AudioSource>();
        performAction = true;
        visor_v = GameObject.FindGameObjectWithTag("VisionVigilante");
        m_collider = visor_v.GetComponent<Collider>();
        speed = 4;
    }

    private void Update()
    {
        stealthBarUI.transform.LookAt(Camera.main.transform); //mirar a cámara siempre
    }


    private void OnTriggerEnter(Collider other) {
        if (en_vigilancia)
        {
            if (other.gameObject.tag == "MainPlayer")
            {

                objetivo_detectado = true;
                Debug.Log("################  COLISION  ####################");
                // vigilante.transform.LookAt(target.transform);
                // transform.LookAt(target.transform);


            }
        }

    }

    private void OnTriggerExit(Collider other)
    {
        en_vigilancia = true;

    }

    // private void OnTriggerExit(Collider other) {
    //   if (other.gameObject.tag == "MainPlayer") {
    //     en_vigilancia = false;
    //   }
    // }

    // Update is called once per frame
    void FixedUpdate()
    {
        //objetivo_detectado = true;
        //vigilante.transform.LookAt(target.transform);
        //transform.LookAt(target.transform);

        //==========================================================================================
        if (objetivo_detectado) {
            vigilante.transform.LookAt(target.transform);
            vigilante.transform.Rotate(2,0,0,Space.Self);
        } else {

            time += Time.deltaTime;

            if (time > speed)
            {
                targetDir = target.transform.position - vigilante.transform.position;
                angle = -Vector3.SignedAngle(targetDir, vigilante.transform.forward, Vector3.up);

                Debug.Log("ANGLE :::::::::: " + angle);

                if (vigilante.transform.rotation.x > 0)
                    angulo_global_x = Random.Range(-20, 0);
                else
                    angulo_global_x = Random.Range(0, 20);

                angulo_global_y = Random.Range(angle-10, angle+10);

                time = 0.0f;
            }

            angulo_parcial_x += (angulo_global_x/(speed/Time.deltaTime));
            angulo_parcial_y += (angulo_global_y/(speed/Time.deltaTime));

            vigilante.transform.localRotation = Quaternion.Euler(angulo_parcial_x, angulo_parcial_y, 0);
        }

        //==========================================================================================

        RaycastHit objectHit;
        Vector3 fwd = transform.TransformDirection(Vector3.forward);

        //vigilante.transform.LookAt(target.transform);
        //transform.LookAt(target.transform);
        //vigilante.transform.Rotate(-3, 0, 0, Space.Self);


        if (objetivo_detectado)
        {
            m_collider.enabled = false;

            Debug.DrawRay(visor_v.transform.position, fwd * 60, Color.red); //35 metros de alcance metros max
            if (Physics.Raycast(visor_v.transform.position, fwd * 60, out objectHit, 70))
            {
                //Debug.Log(objectHit.collider.name);
                if (objectHit.transform.gameObject.tag.Equals("MainPlayer"))
                {
                    counts++;
                    Debug.Log(" +++ See Player increment count... " + counts);
                    Debug.Log("DETECTADO CON RAYO");
                    if (counts % 50 == 0)
                    {
                        if (!stealthBarUI.activeSelf)
                            stealthBarUI.SetActive(true);

                        slider.value += 0.05f;

                        if (!audioSourceEmptyObject.isPlaying)
                        {
                            Debug.Log("Play TICK SOUND");
                            audioSourceEmptyObject.clip = sonidoTick;
                            audioSourceEmptyObject.volume = 0.7f;
                            audioSourceEmptyObject.priority = 20;
                            audioSourceEmptyObject.Play();
                        }

                    }
                    if (counts % 1000 == 0)
                    {
                        Debug.Log("Play ALARM SOUND");

                        //Do important Stuff...
                        buffEnemies();

                        Debug.Log("Vigilancia pasa a estar inactiva");
                        counts = 0;
                        performAction = false;
                        audioSourceEmptyObject.clip = sonidoAlarm;
                        audioSourceEmptyObject.volume = 0.2f;
                        audioSourceEmptyObject.priority = 20;
                        audioSourceEmptyObject.Play();
                        Invoke("restoreFunction", 15f);
                    }
                }
                else
                {
                    decreaseCounts();
                    Debug.Log("NO SE DETECTA AL MAINPLAYER");
                }

            }

        }


        //if (en_vigilancia) {

        //    vigilante.transform.LookAt(target.transform);
        //    transform.LookAt(target.transform);
        //    vigilante.transform.Rotate(-3, 0, 0, Space.Self);

        //    if (Physics.Raycast(visor_v.transform.position, fwd * 60, out objectHit, 70))
        //    {
        //        if (objectHit.collider.tag.Equals("MainPlayer"))
        //        {
        //            Debug.Log("DETECTADO CON RAYO");
        //        }

        //    }

        //    if (Physics.Raycast(vigilante.transform.position, target.transform.position, out objectHit, 70) && performAction)
        //    {
        //        //do something if hit objective
        //        if (objectHit.transform.gameObject.tag == "MainPlayer")
        //        {
        //            objetivo_detectado = true;
        //            counts++;
        //            Debug.Log("See Player increment count... " + counts);
        //            if (counts % 50 == 0)
        //            {
        //                if (!stealthBarUI.activeSelf)
        //                    stealthBarUI.SetActive(true);

        //                slider.value += 0.05f;

        //                if (!audioSourceEmptyObject.isPlaying)
        //                {
        //                    Debug.Log("Play TICK SOUND");
        //                    audioSourceEmptyObject.clip = sonidoTick;
        //                    audioSourceEmptyObject.volume = 0.7f;
        //                    audioSourceEmptyObject.priority = 20;
        //                    audioSourceEmptyObject.Play();
        //                }

        //            }
        //            if (counts % 1000 == 0)
        //            {
        //                Debug.Log("Play ALARM SOUND");

        //                //Do important Stuff...
        //                buffEnemies();

        //                Debug.Log("Vigilancia pasa a estar inactiva");
        //                counts = 0;
        //                performAction = false;
        //                audioSourceEmptyObject.clip = sonidoAlarm;
        //                audioSourceEmptyObject.volume = 0.2f;
        //                audioSourceEmptyObject.priority = 20;
        //                audioSourceEmptyObject.Play();
        //                Invoke("restoreFunction", 15f);
        //            }
        //        } else
        //        {
        //            decreaseCounts();
        //        }

        //    } else if (performAction)

        //    {
        //        decreaseCounts();
        //    }
        //}
    }

    void decreaseCounts ()
    {
        counts--;
        Debug.Log(" --- See Player decrement count... " + counts);
        if (counts > 0)
        {
            if (counts % 50 == 0)
                slider.value -= 0.05f;
        }
        else
        {
            counts = 0;
            slider.value = 0;
            objetivo_detectado = false;
            en_vigilancia = false;
            m_collider.enabled = true;
            if (stealthBarUI.activeSelf)
                stealthBarUI.SetActive(false);
        }
    }

    void buffEnemies ()
    {
        enemiesArray = GameObject.FindGameObjectsWithTag("EnemyBerserk");

        foreach (GameObject enemy in enemiesArray)
        {
            ThirdPersonCharacter componentEnemy = enemy.GetComponent<ThirdPersonCharacter>();
            componentEnemy.m_MoveSpeedMultiplier *= 2;
            componentEnemy.damagePerDeltaTime *= 4;
        }
    }

    void restoreEnemies ()
    {
        enemiesArray = GameObject.FindGameObjectsWithTag("EnemyBerserk");

        foreach (GameObject enemy in enemiesArray)
        {
            ThirdPersonCharacter componentEnemy = enemy.GetComponent<ThirdPersonCharacter>();
            componentEnemy.m_MoveSpeedMultiplier /= 2;
            componentEnemy.damagePerDeltaTime /= 4;
        }
    }

    void restoreFunction ()
    {
        audioSourceEmptyObject.clip = sonidoEnfado;
        audioSourceEmptyObject.volume = 0.2f;
        audioSourceEmptyObject.priority = 20;
        audioSourceEmptyObject.Play();

        slider.value = 0;
        if (stealthBarUI.activeSelf)
            stealthBarUI.SetActive(false);

        Debug.Log("Vigilancia activa de nuevo...");
        performAction = true;

        //restore enemies state...
        restoreEnemies();
    }
}

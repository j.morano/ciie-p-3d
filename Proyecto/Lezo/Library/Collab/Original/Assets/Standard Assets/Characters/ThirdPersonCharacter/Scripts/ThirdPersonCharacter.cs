using System.Collections.Generic;
using UnityEngine;

namespace UnityStandardAssets.Characters.ThirdPerson
{
    [RequireComponent(typeof(Rigidbody))]
	[RequireComponent(typeof(CapsuleCollider))]
	[RequireComponent(typeof(Animator))]
	public class ThirdPersonCharacter : MonoBehaviour
	{
		[SerializeField] float m_MovingTurnSpeed = 360;
		[SerializeField] float m_StationaryTurnSpeed = 180;
		[SerializeField] float m_JumpPower = 12f;
		[Range(1f, 4f)][SerializeField] float m_GravityMultiplier = 2f;
		[SerializeField] float m_RunCycleLegOffset = 0.2f; //specific to the character in sample assets, will need to be modified to work with others
		[SerializeField] public float m_MoveSpeedMultiplier = 1f;
		[SerializeField] float m_AnimSpeedMultiplier = 1f;
		[SerializeField] float m_GroundCheckDistance = 0.1f;

		Rigidbody m_Rigidbody;
		Animator m_Animator;
        Animation animation;
        public bool m_IsGrounded;
		float m_OrigGroundCheckDistance;
		const float k_Half = 0.5f;
		float m_TurnAmount;
		float m_ForwardAmount;
		Vector3 m_GroundNormal;
		float m_CapsuleHeight;
		Vector3 m_CapsuleCenter;
		CapsuleCollider m_Capsule;
		bool m_Crouching;
        bool alreadyDead;
        bool onlyProcessDeathOnce;
        bool isInAttackRange;

        List <GameObject> objectives = new List<GameObject>();

        public float health; //Vida del protagonista o de los enemigos de la IA
        public float maxHealth; //Vida maxima del personaje.

        public float damagePerDeltaTime;
        public float areaRangeCombat; //Rango de ataque en arco;
        private Vector3 rayDirection = Vector3.zero;

        bool stopPlaying = false;
        public AudioClip SonidoEspada;
        public AudioClip SonidoMuerte;
        public AudioClip SonidoGameOver;
        public AudioClip SonidoDano;

        private AudioSource audioReference;
        private AudioSource [] allAudioSources;

        public bool hasKey;
        public AudioClip doorOpen;
        private int acuTmpDoor;
        private GameObject propInteract;

        private void Awake()
        {
            audioReference = GetComponent<AudioSource>();
            hasKey = false;
            acuTmpDoor = 0;
        }

        //Parar todos los audioSource existentes
        void StopAllAudio()
        {
            allAudioSources = FindObjectsOfType(typeof(AudioSource)) as AudioSource[];

            foreach (AudioSource audioS in allAudioSources)
            {
                audioS.Stop();
            }
        }

        void Start()
		{
			m_Animator = GetComponent<Animator>();
			m_Rigidbody = GetComponent<Rigidbody>();
			m_Capsule = GetComponent<CapsuleCollider>();
            animation = GetComponent<Animation>();
            m_CapsuleHeight = m_Capsule.height;
			m_CapsuleCenter = m_Capsule.center;

			m_Rigidbody.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezeRotationZ;
			m_OrigGroundCheckDistance = m_GroundCheckDistance;

            alreadyDead = false;
            onlyProcessDeathOnce = false;
            isInAttackRange = false;

            maxHealth = health;
        }


		public void Move(Vector3 move, bool crouch, bool jump, bool attack)
		{

            // convert the world relative moveInput vector into a local-relative
            // turn amount and forward amount required to head in the desired
            // direction.


            if (attack)
            {
                if (!(m_Animator.GetCurrentAnimatorStateInfo(0).IsName("Attack")))
                { //Para no sobreescribir la animaci�n de ataque antes de que finalice
                    stopPlaying = false;
                    m_Animator.Play("Attack", 0, 0f);
                    Debug.Log("Attack1 !");
                }
            }

            checkHealth(); //Comprobar vida propia
            reduceHealthStatus(); //reducir vida con los que colisiona y ataca

            if (m_Animator.GetCurrentAnimatorStateInfo(0).IsName("Attack"))
            {
                move.x = 0;
                move.y = 0;
                move.z = 0;
            }


            if (move.magnitude > 1f) move.Normalize();
			move = transform.InverseTransformDirection(move);
			CheckGroundStatus();
			move = Vector3.ProjectOnPlane(move, m_GroundNormal);
			m_TurnAmount = Mathf.Atan2(move.x, move.z);
			m_ForwardAmount = move.z;

			ApplyExtraTurnRotation();

			// control and velocity handling is different when grounded and airborne:
			if (m_IsGrounded)
			{
				HandleGroundedMovement(crouch, jump);
			}
			else
			{
				HandleAirborneMovement();
			}

			ScaleCapsuleForCrouching(crouch);
			PreventStandingInLowHeadroom();

			// send input and other state parameters to the animator
			UpdateAnimator(move);
		}

        void checkHealth()
        {
            if (health>maxHealth)
            {
                health = maxHealth; //No puedes curarte m�s de la salud m�xima
            }

            if (health <= 0 && !alreadyDead)
            {
                if (gameObject.tag == "MainPlayer")
                {
                    gameObject.GetComponent<ThirdPersonUserControl>().enabled = false; //Bloquear todo funcionamiento de controles al morir
                    GameObject[] enemigos = GameObject.FindGameObjectsWithTag("EnemyBerserk");
                    foreach (GameObject enemigo in enemigos) { //evitamos que sigan su comportamiento si el personaje principal ya ha muerto.
                        //Debug.Log(enemigo.ToString());
                        enemigo.GetComponent<AICharacterControl>().enabled = false;
                        //enemigo.GetComponent<ThirdPersonCharacter>().Move(new Vector3(0, 0, 0), false, false, false); //parar el personaje
                    }
                }
                if (gameObject.tag == "EnemyBerserk")
                {
                    audioReference.clip = SonidoMuerte;
                    audioReference.volume = 0.5f;
                    audioReference.time = 0.7f;
                    audioReference.Play();
                    gameObject.GetComponent<AICharacterControl>().enabled = false; //Bloquear todo funcionamiento de controles al morir
                    gameObject.tag = "DeadEnemyIgnore"; //Para mientras que se muere ya no sea fijado por el script para calculo de da�o
                }
                if (!m_Animator.GetCurrentAnimatorStateInfo(0).IsName("death"))
                {
                    m_Animator.Play("death", 0, 0f);
                    alreadyDead = true;
                    Invoke("isDead", (float)3);
                }
                //Cambiar a estado del arbol con animacion de muerte
                //Parar la animacion cuando acaba su tiempo y eliminar dicha instancia del modelo de escena.
            }

            /*float currentTime = m_Animator.GetCurrentAnimatorStateInfo(0).normalizedTime % 1;

            if (alreadyDead && currentTime >= 0.85 && !onlyProcessDeathOnce)
            {
                onlyProcessDeathOnce = true;
                isDead();
            }*/

        }

        void isDead()
        {
            Debug.Log("Est� muerto");
            if (gameObject.tag == "MainPlayer")
            {
                Debug.Log("Muri� el protagonista");
                StopAllAudio();
                mainPlayerIsDead();

            } else if (gameObject.tag == "DeadEnemyIgnore")
            {
                Debug.Log("Muri� un berserk");
                Destroy(gameObject);
            }

            //aqui se cargar�a el menu de muerte, por ejemplo.
        }

        void mainPlayerIsDead()
        {
            AudioSource.PlayClipAtPoint(SonidoGameOver, transform.position, 0.5f);
            //Application.Quit(0);
            //Aqui se cargar�a el menu de muerte
        }

        void reduceHealthStatus()
        {

            if (m_Animator.GetCurrentAnimatorStateInfo(0).IsName("Attack"))
            {
                float playbackTime = m_Animator.GetCurrentAnimatorStateInfo(0).normalizedTime % 1;
                if (playbackTime >= 0.45 && playbackTime <= 0.50)
                {
                    if (gameObject.tag == "MainPlayer" && !stopPlaying)
                    {
                        stopPlaying = true;
                        audioReference.volume = 0.3f;
                        audioReference.loop = false;
                        audioReference.priority = 100;
                        audioReference.clip = SonidoEspada;
                        audioReference.Play();
                    }
                    //check also collisions state!
                    //collision variables       //da�o por cercan�a
                    if (IsNearToCauseDamage() || isInAttackRange)
                    {
                        Debug.Log("Now apply Damage");
                        Debug.Log(playbackTime);
                        foreach (GameObject objective in objectives)
                        {
                            if (objective.tag == "MainPlayer")
                            {
                                //tomamos el AudioSource del empty object de su hijo, no el AudioSource del MainPlayer root.
                                AudioSource audioMainPlayerDano = objective.transform.Find("SonidoDano").gameObject.GetComponent<AudioSource>();
                                if (!audioMainPlayerDano.isPlaying)
                                {
                                    audioMainPlayerDano.clip = SonidoDano;
                                    audioMainPlayerDano.priority = 200;
                                    audioMainPlayerDano.loop = false;
                                    audioMainPlayerDano.volume = 0.2f;
                                    audioMainPlayerDano.Play();
                                }
                            }
                            objective.GetComponent<ThirdPersonCharacter>().health -= Time.deltaTime * damagePerDeltaTime;
                        }
                    }
                }
                objectives.Clear(); //Una vez pasada la iteraci�n, se vuelve a vaciar para volver a comprobar el estado de los enemigos
            }
        }

        private bool IsNearToCauseDamage()
        {
            if (gameObject.tag == "MainPlayer")
            {
                GameObject[] enemigos = GameObject.FindGameObjectsWithTag("EnemyBerserk");
                foreach (GameObject enemigo in enemigos)
                {
                    rayDirection = enemigo.transform.position - transform.position;
                    var distanceToEnemy = Vector3.Distance(gameObject.transform.position, enemigo.transform.position);

                    if ((distanceToEnemy <= 2.5) && ((Vector3.Angle(rayDirection, transform.forward)) < areaRangeCombat))
                    {
                        objectives.Add(enemigo);
                        return true;
                    }
                }
            }

            if (gameObject.tag == "EnemyBerserk")
            {
                GameObject personaje = GameObject.FindGameObjectWithTag("MainPlayer");
                if (Vector3.Distance(gameObject.transform.position, personaje.transform.position) <= 2.5)
                {
                    objectives.Add(personaje);
                    return true;
                }
            }

            return false;
        }
        private void OnCollisionEnter(Collision collision)
        {
            //Ataque de un berserk al personaje principal
            if (collision.gameObject.tag=="MainPlayer" && gameObject.tag == "EnemyBerserk")
            {
                Debug.Log("Ataque disponible del berserk: " + gameObject.ToString());
                objectives.Add(collision.gameObject);
                isInAttackRange = true;
            }

            //Ataque del personaje principal a un berserk
            if (collision.gameObject.tag == "EnemyBerserk" && gameObject.tag == "MainPlayer")
            {
                Debug.Log("Ataque disponible de personaje principal: " + gameObject.ToString());
                objectives.Add(collision.gameObject);
                isInAttackRange = true;
            }

            if (hasKey && collision.gameObject.tag == "openDoor")
            {
                propInteract = collision.gameObject.transform.parent.gameObject;
                acuTmpDoor = 0;
                hasKey = false;
                AudioSource.PlayClipAtPoint(doorOpen, collision.transform.position, 1f);
                Invoke("doActionProp", 0.1f);
            }

        }

        private void OnCollisionExit(Collision collision)
        {
            //Ataque de un berserk al personaje principal
            if (collision.gameObject.tag == "MainPlayer" && gameObject.tag == "EnemyBerserk")
            {
                Debug.Log("Ataque NO - disponible del berserk: " + gameObject.ToString());
                isInAttackRange = false;
            }

            //Ataque del personaje principal a un berserk
            if (collision.gameObject.tag == "EnemyBerserk" && gameObject.tag == "MainPlayer")
            {
                Debug.Log("Ataque NO - disponible de personaje principal: " + gameObject.ToString());
                isInAttackRange = false;
            }
        }

        private void doActionProp()
        {
            Debug.Log(acuTmpDoor);
            acuTmpDoor++;
            if (acuTmpDoor >= 160)
            {
                Debug.Log("Stop");
                return;
            }
            propInteract.transform.Rotate(new Vector3(0, -0.5f, 0));
            Invoke("doActionProp", 0f);
        }

        void ScaleCapsuleForCrouching(bool crouch)
		{
			if (m_IsGrounded && crouch)
			{
				if (m_Crouching) return;
				m_Capsule.height = m_Capsule.height / 2f;
				m_Capsule.center = m_Capsule.center / 2f;
				m_Crouching = true;
			}
			else
			{
				Ray crouchRay = new Ray(m_Rigidbody.position + Vector3.up * m_Capsule.radius * k_Half, Vector3.up);
				float crouchRayLength = m_CapsuleHeight - m_Capsule.radius * k_Half;
				if (Physics.SphereCast(crouchRay, m_Capsule.radius * k_Half, crouchRayLength, Physics.AllLayers, QueryTriggerInteraction.Ignore) && gameObject.tag != "EnemyBerserk")
				{
					m_Crouching = true;
					return;
				}
				m_Capsule.height = m_CapsuleHeight;
				m_Capsule.center = m_CapsuleCenter;
				m_Crouching = false;
			}
		}

		void PreventStandingInLowHeadroom()
		{
			// prevent standing up in crouch-only zones
			if (!m_Crouching)
			{
				Ray crouchRay = new Ray(m_Rigidbody.position + Vector3.up * m_Capsule.radius * k_Half, Vector3.up);
				float crouchRayLength = m_CapsuleHeight - m_Capsule.radius * k_Half;
				if (Physics.SphereCast(crouchRay, m_Capsule.radius * k_Half, crouchRayLength, Physics.AllLayers, QueryTriggerInteraction.Ignore))
				{
					m_Crouching = true;
				}
			}
		}


		void UpdateAnimator(Vector3 move)
		{
			// update the animator parameters
			m_Animator.SetFloat("Forward", m_ForwardAmount, 0.1f, Time.deltaTime);
			m_Animator.SetFloat("Turn", m_TurnAmount, 0.1f, Time.deltaTime);
            if (gameObject.tag == "EnemyBerserk")
            {
                m_Crouching = false;    //Evitar que se agache la IA en lugares indeseados.
            }
			m_Animator.SetBool("Crouch", m_Crouching);
			m_Animator.SetBool("OnGround", m_IsGrounded);
			if (!m_IsGrounded)
			{
				m_Animator.SetFloat("Jump", m_Rigidbody.velocity.y);
			}

			// calculate which leg is behind, so as to leave that leg trailing in the jump animation
			// (This code is reliant on the specific run cycle offset in our animations,
			// and assumes one leg passes the other at the normalized clip times of 0.0 and 0.5)
			float runCycle =
				Mathf.Repeat(
					m_Animator.GetCurrentAnimatorStateInfo(0).normalizedTime + m_RunCycleLegOffset, 1);
			float jumpLeg = (runCycle < k_Half ? 1 : -1) * m_ForwardAmount;
			if (m_IsGrounded)
			{
				m_Animator.SetFloat("JumpLeg", jumpLeg);
			}

			// the anim speed multiplier allows the overall speed of walking/running to be tweaked in the inspector,
			// which affects the movement speed because of the root motion.
			if (m_IsGrounded && move.magnitude > 0)
			{
				m_Animator.speed = m_AnimSpeedMultiplier;
			}
			else
			{
				// don't use that while airborne
				m_Animator.speed = 1;
			}
		}


		void HandleAirborneMovement()
		{
			// apply extra gravity from multiplier:
			Vector3 extraGravityForce = (Physics.gravity * m_GravityMultiplier) - Physics.gravity;
			m_Rigidbody.AddForce(extraGravityForce);

			m_GroundCheckDistance = m_Rigidbody.velocity.y < 0 ? m_OrigGroundCheckDistance : 0.01f;
		}


		void HandleGroundedMovement(bool crouch, bool jump)
		{
			// check whether conditions are right to allow a jump:
			if (jump && !crouch && m_Animator.GetCurrentAnimatorStateInfo(0).IsName("Grounded"))
			{
				// jump!
				m_Rigidbody.velocity = new Vector3(m_Rigidbody.velocity.x, m_JumpPower, m_Rigidbody.velocity.z);
				m_IsGrounded = false;
				m_Animator.applyRootMotion = false;
				m_GroundCheckDistance = 0.1f;
			}
		}

		void ApplyExtraTurnRotation()
		{
			// help the character turn faster (this is in addition to root rotation in the animation)
			float turnSpeed = Mathf.Lerp(m_StationaryTurnSpeed, m_MovingTurnSpeed, m_ForwardAmount);
			transform.Rotate(0, m_TurnAmount * turnSpeed * Time.deltaTime, 0);
		}


		public void OnAnimatorMove()
		{
			// we implement this function to override the default root motion.
			// this allows us to modify the positional speed before it's applied.
			if (m_IsGrounded && Time.deltaTime > 0)
			{
				Vector3 v = (m_Animator.deltaPosition * m_MoveSpeedMultiplier) / Time.deltaTime;

				// we preserve the existing y part of the current velocity.
				v.y = m_Rigidbody.velocity.y;
				m_Rigidbody.velocity = v;
			}
		}


		void CheckGroundStatus()
		{
			RaycastHit hitInfo;
#if UNITY_EDITOR
			// helper to visualise the ground check ray in the scene view
			Debug.DrawLine(transform.position + (Vector3.up * 0.1f), transform.position + (Vector3.up * 0.1f) + (Vector3.down * m_GroundCheckDistance));
#endif
			// 0.1f is a small offset to start the ray from inside the character
			// it is also good to note that the transform position in the sample assets is at the base of the character
			if (Physics.Raycast(transform.position + (Vector3.up * 0.1f), Vector3.down, out hitInfo, m_GroundCheckDistance))
			{
				m_GroundNormal = hitInfo.normal;
				m_IsGrounded = true;
				m_Animator.applyRootMotion = true;
			}
			else
			{
				m_IsGrounded = false;
				m_GroundNormal = Vector3.up;
				m_Animator.applyRootMotion = false;
			}
		}
	}
}

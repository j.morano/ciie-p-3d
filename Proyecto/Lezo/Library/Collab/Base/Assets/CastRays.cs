﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.Characters.ThirdPerson;

public class CastRays : MonoBehaviour
{
    // Start is called before the first frame update
    private GameObject target;
    private GameObject target_m;
    private GameObject vigilante;
    private int counts;
    private AudioSource audioSourceEmptyObject;
    public AudioClip sonidoTick;
    public AudioClip sonidoAlarm;
    public AudioClip sonidoEnfado;
    private bool performAction;

    //==========================================================================================
    float time    = 0.0f;
    float angulo_global_x = 0.0f;
    float angulo_parcial_x = 0.0f;
    float angulo_global_y = 0.0f;
    float angulo_parcial_y = 0.0f;
    private bool objetivo_detectado = false;
    float margen = 0.0f;
    //==========================================================================================

    private GameObject[] enemiesArray;

    //Manage stealth bar
    public GameObject stealthBarUI;
    public Slider slider;

    void Start()
    {
        counts = 0;
        target = GameObject.FindGameObjectWithTag("MainPlayer"); //ThirdPerson controller
        vigilante = transform.Find("vigilante").gameObject;
        audioSourceEmptyObject = target.transform.Find("SonidoTick").gameObject.GetComponent<AudioSource>();
        performAction = true;

    }

    private void Update()
    {
        stealthBarUI.transform.LookAt(Camera.main.transform); //mirar a cámara siempre
    }

    // Update is called once per frame
    void FixedUpdate()
    {

        //vigilante.transform.LookAt(target.transform);
        //transform.LookAt(target.transform);

        //==========================================================================================
        if (objetivo_detectado) {
            vigilante.transform.LookAt(target.transform);
            transform.LookAt(target.transform);
        } else {

            time += Time.deltaTime;

            if (time > 5.0f)
            {
                Vector3 targetDir = target.transform.position - vigilante.transform.position;
                float angle = -Vector3.SignedAngle(targetDir, vigilante.transform.forward, Vector3.up);

                Debug.Log("ANGLE :::::::::: " + angle);
                //vigilante.transform.rotation = rotation;

                // angulo_global_x = rotation.x;
                if (angulo_global_x > 0)
                    angulo_global_x = Random.Range(-15-angulo_global_x, -angulo_global_x);
                else
                    angulo_global_x = Random.Range(angulo_global_x, 15-angulo_global_x);

                angulo_global_y = Random.Range(angle-10, angle+10);

                time    = 0.0f;
            }

            angulo_parcial_x += (angulo_global_x/(5/Time.deltaTime));
            angulo_parcial_y += (angulo_global_y/(5/Time.deltaTime));

            vigilante.transform.localRotation = Quaternion.Euler(angulo_parcial_x, angulo_parcial_y, 0);
        }

        //==========================================================================================

        RaycastHit objectHit;
        Vector3 fwd = transform.TransformDirection(Vector3.forward);
        Debug.DrawRay(transform.position, fwd * 35, Color.red); //35 metros de alcance metros max
        if (Physics.Raycast(transform.position, fwd, out objectHit, 35) && performAction)
        {
            //do something if hit objective
            if (objectHit.transform.gameObject.tag == "MainPlayer")
            {
                objetivo_detectado = true;
                counts++;
                Debug.Log("See Player increment count... " + counts);
                if (counts % 50 == 0)
                {
                    if (!stealthBarUI.activeSelf)
                        stealthBarUI.SetActive(true);

                    slider.value += 0.05f;

                    if (!audioSourceEmptyObject.isPlaying)
                    {
                        Debug.Log("Play TICK SOUND");
                        audioSourceEmptyObject.clip = sonidoTick;
                        audioSourceEmptyObject.volume = 0.7f;
                        audioSourceEmptyObject.priority = 20;
                        audioSourceEmptyObject.Play();
                    }

                }
                if (counts % 1000 == 0)
                {
                    Debug.Log("Play ALARM SOUND");

                    //Do important Stuff...
                    buffEnemies();

                    Debug.Log("Vigilancia pasa a estar inactiva");
                    counts = 0;
                    performAction = false;
                    audioSourceEmptyObject.clip = sonidoAlarm;
                    audioSourceEmptyObject.volume = 0.2f;
                    audioSourceEmptyObject.priority = 20;
                    audioSourceEmptyObject.Play();
                    Invoke("restoreFunction", 15f);
                }
            } else
            {
                decreaseCounts();
            }

        } else if (performAction)

        {
            decreaseCounts();
        }
    }

    void decreaseCounts ()
    {
        counts--;
        if (counts > 0)
        {
            if (counts % 50 == 0)
                slider.value -= 0.05f;
        }
        else
        {
            counts = 0;
            slider.value = 0;
            objetivo_detectado = false;
            if (stealthBarUI.activeSelf)
                stealthBarUI.SetActive(false);
        }
    }

    void buffEnemies ()
    {
        enemiesArray = GameObject.FindGameObjectsWithTag("EnemyBerserk");

        foreach (GameObject enemy in enemiesArray)
        {
            ThirdPersonCharacter componentEnemy = enemy.GetComponent<ThirdPersonCharacter>();
            componentEnemy.m_MoveSpeedMultiplier *= 2;
            componentEnemy.damagePerDeltaTime *= 4;
        }
    }

    void restoreEnemies ()
    {
        enemiesArray = GameObject.FindGameObjectsWithTag("EnemyBerserk");

        foreach (GameObject enemy in enemiesArray)
        {
            ThirdPersonCharacter componentEnemy = enemy.GetComponent<ThirdPersonCharacter>();
            componentEnemy.m_MoveSpeedMultiplier /= 2;
            componentEnemy.damagePerDeltaTime /= 4;
        }
    }

    void restoreFunction ()
    {
        audioSourceEmptyObject.clip = sonidoEnfado;
        audioSourceEmptyObject.volume = 0.2f;
        audioSourceEmptyObject.priority = 20;
        audioSourceEmptyObject.Play();

        slider.value = 0;
        if (stealthBarUI.activeSelf)
            stealthBarUI.SetActive(false);

        Debug.Log("Vigilancia activa de nuevo...");
        performAction = true;

        //restore enemies state...
        restoreEnemies();
    }
}

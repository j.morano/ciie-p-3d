﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.ThirdPerson;

public class Floater : MonoBehaviour
{
    // User Inputs
    public float degreesPerSecond = 15.0f;
    public float amplitude = 0.5f;
    public float frequency = 1f;

    public AudioClip getKey;
    public AudioClip doorOpen;

    public GameObject propInteract;
    private int acuTmpDoor;

    public float restoredHealth;

    // Position Storage Variables
    Vector3 posOffset = new Vector3();
    Vector3 tempPos = new Vector3();

    private AudioSource audioReference;

    private void Awake()
    {
        audioReference = GetComponent<AudioSource>();
    }

    // Use this for initialization
    void Start()
    {
        // Store the starting position & rotation of the object
        posOffset = transform.position;
        acuTmpDoor = 0;
    }

    // Update is called once per frame
    void Update()
    {
        // Spin object around Y-Axis
        transform.Rotate(new Vector3(0f, Time.deltaTime * degreesPerSecond, 0f), Space.World);

        // Float up/down with a Sin()
        tempPos = posOffset;
        tempPos.y += Mathf.Sin(Time.fixedTime * Mathf.PI * frequency) * amplitude;

        transform.position = tempPos;
    }

    private GameObject objectParticleReference;

    void restartParticles ()
    {
        objectParticleReference.GetComponent<ParticleSystem>().Stop();
        objectParticleReference.SetActive(false);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "MainPlayer")
        {
            //Reproducirlo de esta manera para evitar que al eneminarlo se pierda la referencia.
            float volumen = 0f;

            if (gameObject.tag == "Potion")
            {
                Debug.Log("Es poción");
                other.gameObject.GetComponent<ThirdPersonCharacter>().health += restoredHealth;
                volumen = 0.05f;
                objectParticleReference = other.gameObject.transform.Find("SaludParticulas").gameObject;
                objectParticleReference.SetActive(true);
                if (!objectParticleReference.GetComponent<ParticleSystem>().isPlaying)
                    objectParticleReference.GetComponent<ParticleSystem>().Play();
                Invoke("restartParticles", 4.0f);

            } else
            {
                Debug.Log("Es llave");
                volumen = 0.3f;
            }

            AudioSource.PlayClipAtPoint(getKey, gameObject.transform.position,volumen);
            Debug.Log("Trigger Collision with Main Player: " + other.gameObject.ToString());

            if (gameObject.tag == "Potion") //Tenemos que controlar la finalizacion de particulas
            {
                if (objectParticleReference.GetComponent<ParticleSystem>().isPlaying)
                {
                    gameObject.SetActive(false); //En este caso el item se deshabilita.
                }
                else
                {
                    Destroy(gameObject);
                }

            } else if (gameObject.tag == "Key") //Si es otro objeto distinto
            {
                /*if (propInteract != null) {

                    gameObject.SetActive(false);
                    //llamar a abrir puerta sonido.
                    AudioSource.PlayClipAtPoint(doorOpen, propInteract.transform.position, 1f);
                    Invoke("doActionProp", 0.1f);
                }
                else */
                //{
                Destroy(gameObject);
                //}
                ThirdPersonCharacter mainPlayer = other.gameObject.GetComponent<ThirdPersonCharacter>();
                mainPlayer.hasKey = true;
                mainPlayer.visualKey.SetActive(true);

            } else
            {
                Destroy(gameObject); //Si es otro objeto distinto
            }
           
        }
    }

    private void doActionProp()
    {
        Debug.Log(acuTmpDoor);
        acuTmpDoor++;
        if (acuTmpDoor >= 280)
        {
            Debug.Log("Stop");
            return;
        }
        propInteract.transform.Rotate(new Vector3(0, 0.5f, 0));
        Invoke("doActionProp",0f);
    }
}

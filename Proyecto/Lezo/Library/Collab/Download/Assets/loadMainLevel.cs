﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class loadMainLevel : MonoBehaviour
{

    public Slider sliderCharge;
    public Text textCharge;

    // Start is called before the first frame update
    void Start()
    {

        if (SceneManager.GetActiveScene().name == "Carga")
        {
            Invoke("cargaNivel", 5f);
        }
        
    }

    void cargaNivel ()
    {
        StartCoroutine(LoadMyAsyncScene());
    }

    IEnumerator LoadMyAsyncScene()
    {

        Application.backgroundLoadingPriority = ThreadPriority.Low;
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync("mapaFase");
        asyncLoad.allowSceneActivation = false;

        // Wait until the asynchronous scene fully loads
        float cont = 0f;
        while (!asyncLoad.isDone)
        {

            if (asyncLoad.progress >= 0.9f)
            {
                sliderCharge.value = asyncLoad.progress;
                textCharge.text = (asyncLoad.progress * 100).ToString("f0") + " %";
                asyncLoad.allowSceneActivation = true;
                sliderCharge.value = 1;
                textCharge.text = (1 * 100).ToString("f0") + " %";
            } else
            {
                cont += 0.05f;
                sliderCharge.value = cont;
                if (cont > 0.9f)
                    cont = 0.9f;
                textCharge.text = (cont*100).ToString("f0") + " %";

            }
            yield return null;
        }

       

    }
    // Update is called once per frame
    void Update()
    {
        
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace camaraMusica
{
    public class GestionarZonas : MonoBehaviour
    {

        /* La idea de esto sería ponerle una colisión de tipo Trigger para activar la región correspondiente
         * para ir cargando la musica por regiones
         */

        private AudioSource audioReference;
        private AudioSource audioReferencePopUp;

        public AudioClip[] trackMusic;

        public GameObject panel1;
        public GameObject panel2;
        public GameObject panel3;
        public GameObject panel4;
        public AudioClip popUpSound;

        private static GameObject deleteReference;

        private CheckPointCheck checkPointChange;
        private GameObject mainPlayer;

        private void Awake()
        {
            audioReference = GetComponent<AudioSource>();
            audioReferencePopUp = GameObject.Find("SonidoPopUp").GetComponent<AudioSource>();
            mainPlayer = GameObject.FindGameObjectWithTag("MainPlayer");
        }
        // Start is called before the first frame update
        void Start()
        {
            audioReference.loop = true;
            audioReference.volume = 0.05f;
            audioReference.priority = 255;
            audioReference.clip = trackMusic[0];
            audioReference.Play();

            if (panel1 != null)
                panel1.SetActive(false);
            if (panel2 != null)
                panel2.SetActive(false);
            if (panel3 != null)
                panel3.SetActive(false);
            if (panel4 != null)
                panel4.SetActive(false);

            checkPointChange = GameObject.FindGameObjectWithTag("CPoint").GetComponent<CheckPointCheck>();
            
            //Check for checkPoint
            if (checkPointChange.passedCheckPoint)
            {
                mainPlayer.transform.position = checkPointChange.lastCheckpointPosition;
                Debug.Log("Loaded checkpoint at: " + checkPointChange.lastCheckpointPosition);
            }
        }

        public void gameOver()
        {
            audioReference.loop = false;
            audioReference.volume = 0.3f;
            audioReference.priority = 128;
            audioReference.clip = trackMusic[1];
            audioReference.Play();

        }
        // Update is called once per frame
        void Update()
        {

        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.name == "MusicZone1")
            {
                checkPointChange.lastCheckpointPosition = mainPlayer.transform.position;
                checkPointChange.passedCheckPoint = true;
                Debug.Log("Reached checkPoint at Zone 3");

                //También es el panel 3 de información
                Debug.Log("Reached MusicZone1");
                Debug.Log("Reached HelpPanel3");
                Destroy(other.gameObject);
                audioReference.loop = true;
                audioReference.volume = 0.05f;
                audioReference.priority = 255;
                audioReference.clip = trackMusic[2];
                audioReference.Play();

                panel3.SetActive(true);
                if (deleteReference != null && deleteReference.activeSelf)
                    deleteReference.SetActive(false);
                deleteReference = panel3;
                Invoke("destroyText", 10f);
                audioReferencePopUp.loop = false;
                audioReferencePopUp.volume = 0.5f;
                audioReferencePopUp.priority = 128;
                audioReferencePopUp.clip = popUpSound;
                audioReferencePopUp.Play();
            }

            if (other.gameObject.name == "HelpPanel1")
            {
                checkPointChange.lastCheckpointPosition = mainPlayer.transform.position;
                checkPointChange.passedCheckPoint = true;
                Debug.Log("Reached checkPoint at Zone 1");

                Debug.Log("Reached HelpPanel1");
                Destroy(other.gameObject);

                panel1.SetActive(true);
                if (deleteReference != null && deleteReference.activeSelf)
                    deleteReference.SetActive(false);
                deleteReference = panel1;
                Invoke("destroyText", 10f);
                audioReferencePopUp.loop = false;
                audioReferencePopUp.volume = 0.5f;
                audioReferencePopUp.priority = 128;
                audioReferencePopUp.clip = popUpSound;
                audioReferencePopUp.Play();
            }

            if (other.gameObject.name == "HelpPanel2")
            {
                checkPointChange.lastCheckpointPosition = mainPlayer.transform.position;
                checkPointChange.passedCheckPoint = true;
                Debug.Log("Reached checkPoint at Zone 2");

                Debug.Log("Reached HelpPanel2");
                Destroy(other.gameObject);

                panel2.SetActive(true);
                if (deleteReference != null && deleteReference.activeSelf)
                    deleteReference.SetActive(false);
                deleteReference = panel2;
                Invoke("destroyText", 10f);
                audioReferencePopUp.loop = false;
                audioReferencePopUp.volume = 0.5f;
                audioReferencePopUp.priority = 128;
                audioReferencePopUp.clip = popUpSound;
                audioReferencePopUp.Play();
            }

            if (other.gameObject.name == "HelpPanel4")
            {
                checkPointChange.lastCheckpointPosition = mainPlayer.transform.position;
                checkPointChange.passedCheckPoint = true;
                Debug.Log("Reached checkPoint at Zone 4");

                Debug.Log("Reached HelpPanel4");
                Destroy(other.gameObject);

                panel4.SetActive(true);
                if (deleteReference != null && deleteReference.activeSelf)
                    deleteReference.SetActive(false);
                deleteReference = panel4;
                Invoke("destroyText", 10f);
                audioReferencePopUp.loop = false;
                audioReferencePopUp.volume = 0.5f;
                audioReferencePopUp.priority = 128;
                audioReferencePopUp.clip = popUpSound;
                audioReferencePopUp.Play();
            }
        }

        private void destroyText()
        {
            deleteReference.SetActive(false);
        }
    }
}
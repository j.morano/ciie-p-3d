using System;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

namespace UnityStandardAssets.Characters.ThirdPerson
{
    [RequireComponent(typeof (ThirdPersonCharacter))]
    public class ThirdPersonUserControl : MonoBehaviour
    {
        private ThirdPersonCharacter m_Character; // A reference to the ThirdPersonCharacter on the object
        private Transform m_Cam;                  // A reference to the main camera in the scenes transform
        private Vector3 m_CamForward;             // The current forward direction of the camera
        private Vector3 m_Move;
        private bool m_Jump;                      // the world-relative desired move direction, calculated from the camForward and user input.
        private bool m_Attack;
        
        private void Start()
        {
            // get the transform of the main camera
            if (Camera.main != null)
            {
                m_Cam = Camera.main.transform;
            }

            // get the third person character
            m_Character = GetComponent<ThirdPersonCharacter>();
        }

        // Captura de controles desde el update para generar los comandos.
        private void Update()
        {
            if (!m_Jump)
            {
                m_Jump = CrossPlatformInputManager.GetButtonDown("Jump");
            }

            if (!m_Attack)
            {
                m_Attack = CrossPlatformInputManager.GetButtonDown("Fire1");
            }
        }


        // Fixed update is called in sync with physics
        private void FixedUpdate()
        {
            // lectura input (ejes principales para movimiento y si se agacha o no se agacha)
            float h = CrossPlatformInputManager.GetAxis("Horizontal");
            float v = CrossPlatformInputManager.GetAxis("Vertical");
            bool crouch = Input.GetKey(KeyCode.C);

            // calcular direcci�n de movimiento para los personajes.
            if (m_Cam != null)
            {
                // calcular direcci�n de movimiento de la c�mara ante avance
                m_CamForward = Vector3.Scale(m_Cam.forward, new Vector3(1, 0, 1)).normalized;
                m_Move = v*m_CamForward + h*m_Cam.right;
            }
            else
            {
                // direcci�n relativa al mundo sin c�mara principal (si se da el caso).
                m_Move = v*Vector3.forward + h*Vector3.right;
            }
#if !MOBILE_INPUT
			// multiplicador de velocidad de movimiento.
	        if (Input.GetKey(KeyCode.LeftShift)) m_Move *= 0.5f;
#endif
            // pasar todos los comandos al controlador de tercera persona
            // pasar tambi�n los comandos de ataque.
            if (CrossPlatformInputManager.GetAxis("Vertical") < 0)
            { //no permitimos el movimiento negativo, marcha atr�s.
                m_Move.x = 0;
                m_Move.y = 0;
                m_Move.z = 0;
            }
            //Siguiendo los principios del patr�n comando, tomamos cada uno de ellos para enviarlo al controlador.
            m_Character.Move(m_Move, crouch, m_Jump, m_Attack);
            m_Jump = false; //Deshabilitar comandos temporales ante evento.
            m_Attack = false;
        }
    }
}

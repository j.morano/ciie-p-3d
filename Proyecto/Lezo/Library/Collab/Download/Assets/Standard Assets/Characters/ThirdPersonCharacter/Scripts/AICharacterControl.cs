using System;
using UnityEngine;

namespace UnityStandardAssets.Characters.ThirdPerson
{
    [RequireComponent(typeof (UnityEngine.AI.NavMeshAgent))]
    [RequireComponent(typeof (ThirdPersonCharacter))]
    public class AICharacterControl : MonoBehaviour
    {
        public UnityEngine.AI.NavMeshAgent agent { get; private set; } // the navmesh agent required for the path finding
        public ThirdPersonCharacter character { get; private set; } // the character we are controlling
        public Transform target;                                    // target to aim for
        public int maxDistance;

        private long deltaTimeAcu;

        //Para la funcion "seePlayer" IMPLEMENTACI�N ANTIGUA.
        public float fieldOfViewRange;  // campo de vision del enemigo
        public float minPlayerDetectDistance; // distancia desde atras a la que se puede acercar sin ser detectado.
        private float rayRange; // lo que puede ver delante de el
        private Vector3 rayDirection = Vector3.zero;
        private bool on_view = false;

        private void Start()
        {
            // get the components on the object we need.
            agent = GetComponentInChildren<UnityEngine.AI.NavMeshAgent>();
            character = GetComponent<ThirdPersonCharacter>();

	        agent.updateRotation = false;
	        agent.updatePosition = true;

            rayRange = maxDistance; //fijamos la distancia de partida de vision del enemigo.

            deltaTimeAcu = 0;
        }


        private void Update()
        {
            //fijamos objetivo din�mico sobre navMesh del agente.
            if (target != null)
            {
                agent.SetDestination(target.position);
            }

            if (seePlayer()) //Si cumple condiciones.
            {
                if ((Vector3.Distance(transform.position, target.position) > maxDistance))
                    agent.isStopped = true; //Si supera la distancia m�xima se para.
                else
                    agent.isStopped = false; //En otro caso sigue.

                if ((agent.remainingDistance > agent.stoppingDistance))
                    character.Move(agent.desiredVelocity, false, false, false); // Si la distancia faltante es mayor a la condici�n de parada.
                else                                                            // Se env�an los comandos al controlador (en este caso solo el movimiento del agente).
                {
                    character.Move(Vector3.zero, false, false, true); //Si cumple con la condicion de parada, se env�an los comandos al controlador (no se mueve, pero ataca)
                    if (target.GetComponent<ThirdPersonCharacter>().m_IsGrounded) //Si el objetivo se encuentra en el suelo...
                        transform.LookAt(target); //ataca y lo fija nuevamente si est� en el suelo.
                }
            }
            else //Si no cumple condiciones no se mueve ni se envian comandos al controlador.
            {
                agent.isStopped = true;
                character.Move(Vector3.zero, false, false, false);
            }
        }

        //Las condiciones de navegaci�n ahora dependen de los colliders de tipo trigger, "Zonas de influencia" en la memoria.
        private void OnTriggerEnter(Collider other)
        {
            //Si el objetivo entra en su zona de influencia se activa todo el comportamiento superior.
            if (other.gameObject.tag == "MainPlayer")
            {
                on_view = true;
            }
        }

        private void OnTriggerExit(Collider other)
        {
            //Si el objetivo consigue salir de la zona de influencia, se desactiva el comportamiento superior.
            if (other.gameObject.tag == "MainPlayer")
            {
                on_view = false;
            }
        }


        // Raycasts to delete. Cambio de implementaci�n.
        private bool seePlayer()
        {
            return on_view;

            // --- Ahora simplemente se consideran las regiones de colliders y ahorramos todos los costes de gesti�n
            // --- de rayCast. Todo este detalle tambi�n figura en la memoria en la secci�n correspondiente a algoritmos


            //--- IMPLEMENTACI�N ANTIGUA COMENTADA EN MEMORIA ---

            //RaycastHit hit;
            //rayDirection = target.transform.position - transform.position;
            //var distanceToPlayer = Vector3.Distance(transform.position, target.transform.position);

            //if (Physics.Raycast(transform.position, rayDirection, out hit))
            //{ // If the player is very close behind the enemy and not in view the enemy will detect the player
            //    if ((hit.transform.gameObject.tag == "MainPlayer") && (distanceToPlayer <= minPlayerDetectDistance)){
            //        //Debug.Log("Caught player sneaking up behind!");
            //        return true;
            //    }
            //}

            //if ((Vector3.Angle(rayDirection, transform.forward)) < fieldOfViewRange){ // Detect if player is within the field of view
            //    if (Physics.Raycast(transform.position, rayDirection, out hit, rayRange))
            //    {
            //        if (hit.transform.gameObject.tag == "MainPlayer")
            //        {
            //            //Debug.Log("Can see player");
            //            return true;
            //        }
            //        else
            //        {
            //            //Debug.Log("Can not see player");
            //            return false;
            //        }
            //    }
            //}
            //return false; 
        }

        public void SetTarget(Transform target)
        {
            this.target = target;
        }
    }
}

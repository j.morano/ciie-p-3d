﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseSettings : MonoBehaviour
{
    public static bool PauseActive = false;
    public GameObject pauseMenu;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            Debug.Log("Pressed Pause");
            if (PauseActive)
                ResumeGame();
            else
                PauseGame();
        }
    }

    public void ResumeGame()
    {
        pauseMenu.SetActive(false);
        Time.timeScale = 1f;
        PauseActive = false;
    }

    private void PauseGame()
    {
        PauseActive = true;
        Time.timeScale = 0f;
        pauseMenu.SetActive(true);
    }
}

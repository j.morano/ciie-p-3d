﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class OptionsSettings : MonoBehaviour
{
    public AudioMixer audioMixer;

    public Dropdown qualityDropdown;
    private string[] qualityLevels;

    public Dropdown resolutionDropdown;
    private Resolution[] resolutions;

    public Toggle fullScreenToggle;

    public Slider masterVolumeSlider;

    private void StartQualityDropdown()
    {
        Debug.Log("Empieza aqui");
        qualityLevels = QualitySettings.names;

        qualityDropdown.ClearOptions();
        qualityDropdown.AddOptions(new List<string>(qualityLevels));
        qualityDropdown.SetValueWithoutNotify(QualitySettings.GetQualityLevel());
        qualityDropdown.RefreshShownValue();
    }

    private void StartResolutionDrowdown()
    {
        resolutions = Screen.resolutions;

        resolutionDropdown.ClearOptions();

        List<string> options = new List<string>();

        int currentResolutionIndex = 0;

        for (int i = 0; i < resolutions.Length; i++)
        {
            string option = resolutions[i].width + " x " + resolutions[i].height;
            options.Add(option);

            if (resolutions[i].width == Screen.currentResolution.width &&
                resolutions[i].height == Screen.currentResolution.height)
                currentResolutionIndex = i;
        }

        resolutionDropdown.AddOptions(options);

        Debug.Log("Aquí llega");
        resolutionDropdown.SetValueWithoutNotify(currentResolutionIndex);
        resolutionDropdown.RefreshShownValue();
    }

    private void StartFullScreenToogle()
    {
        Debug.Log("FullScreen starts in : " + Screen.fullScreen);
        fullScreenToggle.SetIsOnWithoutNotify(Screen.fullScreen);
    }

    private void StartMasterVolumeSlider()
    {
        Debug.Log("MasterVolume starts in: " + AudioListener.volume);
        masterVolumeSlider.SetValueWithoutNotify(AudioListener.volume);
    }

    public void Start()
    {
        StartQualityDropdown();
        StartResolutionDrowdown();
        StartFullScreenToogle();
        StartMasterVolumeSlider();
    }


    public void setQualitySettings(int qualitySettingsIndex)
    {
        QualitySettings.SetQualityLevel(qualitySettingsIndex);
        # if UNITY_EDITOR
            Debug.Log("Quality Level changed: " + QualitySettings.GetQualityLevel());
        #endif
    }

    public void setResolution(int resolutionIndex)
    {
        Resolution newResolution = resolutions[resolutionIndex];
        Screen.SetResolution(newResolution.width, newResolution.height, Screen.fullScreen);
    }

    public void setFullScreen(bool fullScreen)
    {
        #if UNITY_EDITOR
             Debug.Log("Full screen: " + fullScreen);
        #else
                    Screen.fullScreen = fullScreen;
        #endif
    }
    public void setVolume(float newVolume)
    {
        AudioListener.volume = newVolume;
        //audioMixer.SetFloat("MainVolume", newVolume);
        #if UNITY_EDITOR
            Debug.Log("MainVolume: " + newVolume);
        #endif
    }
}

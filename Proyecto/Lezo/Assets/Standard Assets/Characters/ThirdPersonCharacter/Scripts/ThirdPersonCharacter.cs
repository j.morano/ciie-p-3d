using System.Collections.Generic;
using UnityEngine;

namespace UnityStandardAssets.Characters.ThirdPerson
{
    [RequireComponent(typeof(Rigidbody))]
	[RequireComponent(typeof(CapsuleCollider))]
	[RequireComponent(typeof(Animator))]
	public class ThirdPersonCharacter : MonoBehaviour
	{
        // Controlador genérico de tercera persona compartido entre personaje principal e IA.
        /* Recibe los comandos de los módulos superiores (entrada de eventos para personaje principal) o IA de los enemigos
         * Ajusta los componentes y valores de los modelos que sean necesarios
         * Envía y notifica las actualizaciones al animador modelado por árbol de estados.
         */ 
		[SerializeField] float m_MovingTurnSpeed = 360;
		[SerializeField] float m_StationaryTurnSpeed = 180;
		[SerializeField] float m_JumpPower = 12f;
		[Range(1f, 4f)][SerializeField] float m_GravityMultiplier = 2f;
		[SerializeField] float m_RunCycleLegOffset = 0.2f; 
		[SerializeField] public float m_MoveSpeedMultiplier = 1f;
		[SerializeField] float m_AnimSpeedMultiplier = 1f;
		[SerializeField] float m_GroundCheckDistance = 0.1f;

		Rigidbody m_Rigidbody;
		Animator m_Animator;
        Animation animation;
        public bool m_IsGrounded;
		float m_OrigGroundCheckDistance;
		const float k_Half = 0.5f;
		float m_TurnAmount;
		float m_ForwardAmount;
		Vector3 m_GroundNormal;
		float m_CapsuleHeight;
		Vector3 m_CapsuleCenter;
		CapsuleCollider m_Capsule;
		bool m_Crouching;
        bool alreadyDead;
        bool onlyProcessDeathOnce;
        bool isInAttackRange;

        List <GameObject> objectives = new List<GameObject>();

        public float health; //Vida del protagonista o de los enemigos de la IA
        public float maxHealth; //Vida maxima del personaje.

        public float damagePerDeltaTime; //Daño por tick en el tiempo.
        public float areaRangeCombat; //Rango de ataque en arco;
        private Vector3 rayDirection = Vector3.zero;

        bool stopPlaying = false;
        public AudioClip SonidoEspada;
        public AudioClip SonidoMuerte;
        public AudioClip SonidoGameOver;
        public AudioClip SonidoDano;

        private AudioSource audioReference;
        private AudioSource [] allAudioSources;

        public bool hasKey;
        public bool hasSecondKey;
        public GameObject visualKey;
        public GameObject visualSecondKey;

        public AudioClip doorOpen;
        private int acuTmpDoor;
        private GameObject propInteract;

        public GameObject deadMenu;

        public GameObject finalKey;

        private void Awake()
        {
            //Inicialización inicial de componentes y del estado del HUD.
            audioReference = GetComponent<AudioSource>();
            hasKey = false;
            hasSecondKey = false;
            if (gameObject.tag == "MainPlayer")
            {
                visualKey.SetActive(false);
                visualSecondKey.SetActive(false);
            }
            acuTmpDoor = 0;
        }

        //Parar todos los audioSource existentes
        //Se utiliza cuando se produce la muerte del protagonista.
        void StopAllAudio()
        {
            allAudioSources = FindObjectsOfType(typeof(AudioSource)) as AudioSource[];

            foreach (AudioSource audioS in allAudioSources)
            {
                audioS.Stop();
            }
        }

        //Inicialización de componentes principales para el controlador.
        //Inicialización de variables de estado para los personajes.
        void Start()
		{
			m_Animator = GetComponent<Animator>();
			m_Rigidbody = GetComponent<Rigidbody>();
			m_Capsule = GetComponent<CapsuleCollider>();
            animation = GetComponent<Animation>();
            m_CapsuleHeight = m_Capsule.height;
			m_CapsuleCenter = m_Capsule.center;

			m_Rigidbody.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezeRotationZ;
			m_OrigGroundCheckDistance = m_GroundCheckDistance;

            alreadyDead = false;
            onlyProcessDeathOnce = false;
            isInAttackRange = false;

            maxHealth = health;
        }

        //Método que funciona como interfaz de paso de comandos sobre los módulos superiores de control (IA y teclado).
		public void Move(Vector3 move, bool crouch, bool jump, bool attack)
		{

            if (attack)
            {   //Se notifica la transición de estado al animador con el arbol de estados de animación
                if (!(m_Animator.GetCurrentAnimatorStateInfo(0).IsName("Attack")))
                { //Para no sobreescribir la animacion de ataque antes de que finalice
                    stopPlaying = false;
                    m_Animator.Play("Attack", 0, 0f);
                }
            }

            //Los siguientes métodos aplican a cualquier relación enemigos-personaje, personaje-enemigos. Reutilización.
            checkHealth(); //Comprobar vida propia
            reduceHealthStatus(); //reducir vida con los que colisiona y ataca

            //El controlador se encarga de evitar movimiento durante ataque, aunque así lo indique el comando enviado. Filtrado.
            if (m_Animator.GetCurrentAnimatorStateInfo(0).IsName("Attack"))
            {
                move.x = 0;
                move.y = 0;
                move.z = 0;
            }


            if (move.magnitude > 1f) move.Normalize(); //valores de los ejes recibidos siempre normalizados.
			move = transform.InverseTransformDirection(move);
			CheckGroundStatus(); //Estado respecto al suelo.
			move = Vector3.ProjectOnPlane(move, m_GroundNormal);
			m_TurnAmount = Mathf.Atan2(move.x, move.z);
			m_ForwardAmount = move.z;

			ApplyExtraTurnRotation(); //Ajuste fino de la rotación del personaje.

			// ajuste de velocidad y control atendiendo al estado del personaje (en el suelo o saltando).
			if (m_IsGrounded)
			{
				HandleGroundedMovement(crouch, jump); //si está en el suelo se puede gestionar agacharse y saltar.
			}
			else
			{
				HandleAirborneMovement(); //En el aire se gestiona la caída de gravedad y la velocidad.
			}

			ScaleCapsuleForCrouching(crouch); //Ajuste de la cápsula collider si se está agachado (para la vigilancia).
			PreventStandingInLowHeadroom(); //Evitar que se pueda recuperar el estado recto si no hay espacio para ello.

			//Notificación al animador de los cambios sufridos en el estado interno para efecutar las transiciones que considere
            //en el arbol de estados de animación.
			UpdateAnimator(move);
		}

        //Comprobación de salud del personaje propio.
        void checkHealth()
        {
            if (health>maxHealth)
            {
                health = maxHealth; //No puedes curarte más de la salud máxima
            }

            //Si no tenemos salud y no hemos efectuado todavía su gestión...
            if (health <= 0 && !alreadyDead)
            {
                if (gameObject.tag == "MainPlayer")
                {
                    gameObject.GetComponent<ThirdPersonUserControl>().enabled = false; //Bloquear todo funcionamiento de controles al morir
                    GameObject[] enemigos = GameObject.FindGameObjectsWithTag("EnemyBerserk");
                    foreach (GameObject enemigo in enemigos) { //evitamos que sigan su comportamiento si el personaje principal ya ha muerto.
                        enemigo.GetComponent<AICharacterControl>().enabled = false; //Bloqueamos IAs.
                    }
                }

                /*Si es un enemigo:
                 *  -Reproduce sonido de muerte
                 *  -Parar su IA.
                 *  -Cambiar su tag para formar parte del grupo de enemigos muertos (no considerarlo en esos instantes como objetivo potencial).
                 *  Supone simplificar la gestión de listas de enemigos cercanos.
                 */ 

                if (gameObject.tag == "EnemyBerserk")
                {
                    audioReference.clip = SonidoMuerte;
                    audioReference.volume = 0.5f;
                    audioReference.time = 0.7f;
                    audioReference.Play();
                    gameObject.GetComponent<AICharacterControl>().enabled = false; //Bloquear todo funcionamiento de controles al morir
                    gameObject.tag = "DeadEnemyIgnore"; //Para mientras que se muere ya no sea fijado por el script para calculo de daño
                }

                // Sea el personaje que sea, efectúa su animación de muerte transitando al estado de muerte sumidero.
                if (!m_Animator.GetCurrentAnimatorStateInfo(0).IsName("death"))
                {
                    m_Animator.Play("death", 0, 0f);
                    alreadyDead = true;
                    Invoke("isDead", (float)3); //Gestión de la muerte con retardo (para ver animación).
                }
                //Cambiar a estado del arbol con animacion de muerte
                //Parar la animacion cuando acaba su tiempo.
            }         
        }

        //Se invoca con retardo para gestionar la muerte de los personajes.
        void isDead()
        {
            //Para el personaje principal, se quita toda la música y efectos y se gestiona el menú de muerte.
            if (gameObject.tag == "MainPlayer")
            {
                StopAllAudio();
                mainPlayerIsDead();

            } else if (gameObject.tag == "DeadEnemyIgnore")
            {
                //Para el caso de los enemigos muertos:

                //Si es el alcaide (jefe final) libera la llave para la apertura de puertas final.
                if (transform.parent.gameObject.tag == "Alcaide")
                {
                    if (finalKey != null)
                    {
                        finalKey.transform.position = gameObject.transform.position;
                        finalKey.SetActive(true);
                    }

                }
                //Para cualquier enemigo (incluido el alcaide) se elimina el modelo de la escena.
                Destroy(gameObject);
            }
        }

        //Gestión de la muerte del protagonista.
        void mainPlayerIsDead()
        {
            AudioSource.PlayClipAtPoint(SonidoGameOver, transform.position, 0.5f);

            //Aqui se cargará el menu de muerte
            deadMenu.SendMessage("ChargeDead");

        }

        //Reducir la salud de los personajes a los que se combate (personaje - enemigos, enemigo - personaje).
        void reduceHealthStatus()
        {
            //Si se está en la animación de ataque, se comprueba el intervalo de la animación donde efectuar daño.
            if (m_Animator.GetCurrentAnimatorStateInfo(0).IsName("Attack"))
            {
                float playbackTime = m_Animator.GetCurrentAnimatorStateInfo(0).normalizedTime % 1;

                //Si se está en el intervalo de animación deseado (golpe con arma).
                if (playbackTime >= 0.45 && playbackTime <= 0.50)
                {
                    //Golpe de espada del protagonista y evitar que suene más de una vez en la animación.
                    if (gameObject.tag == "MainPlayer" && !stopPlaying)
                    {
                        stopPlaying = true;
                        audioReference.volume = 0.3f;
                        audioReference.loop = false;
                        audioReference.priority = 100;
                        audioReference.clip = SonidoEspada;
                        audioReference.Play();
                    }

                    //Gestión del combate por cercanía y ángulo o por colisión.
                    //Se complementan ambos casos ya que la gestión de combate por colisión no era lo fina que se esperaba.

                    if (IsNearToCauseDamage() || isInAttackRange)
                    {
                        //Para cada objetivo posible que cumple condiciones.
                        foreach (GameObject objective in objectives)
                        {
                            //Si combaten al personaje principal.
                            if (objective.tag == "MainPlayer")
                            {
                                //tomamos el AudioSource del empty object de su hijo, no el AudioSource del MainPlayer root.
                                AudioSource audioMainPlayerDano = objective.transform.Find("SonidoDano").gameObject.GetComponent<AudioSource>();
                                if (!audioMainPlayerDano.isPlaying)
                                {
                                    //Reproducir sonido de daño ante golpe recibido.
                                    audioMainPlayerDano.clip = SonidoDano;
                                    audioMainPlayerDano.priority = 200;
                                    audioMainPlayerDano.loop = false;
                                    audioMainPlayerDano.volume = 0.2f;
                                    audioMainPlayerDano.Play();
                                }
                            }
                            //Al objetivo (o objetivos) alcanzados decrementarle la salud.
                            objective.GetComponent<ThirdPersonCharacter>().health -= Time.deltaTime * damagePerDeltaTime;
                        }
                    }
                }
                objectives.Clear(); //Una vez pasada la iteracion, se vuelve a vaciar para volver a comprobar el estado de los enemigos
            }
        }

        /* Para el caso del jugador principal:
         *  -Se buscan enemigos que cumplen condiciones de arco de combate y de cercanía y se añaden a la lista de objetivos potenciales.
         *  -Se devuelve true.
         *  
         * Para los enemigos:
         *  -Se comprueba si el personaje principal está en su área de influencia para efectuarle daño y se añade a la lista de objetivos potenciales.
         *  -Se devue true.
         *  
         * En otro caso se devuelve false.
         */ 

        private bool IsNearToCauseDamage()
        {
            if (gameObject.tag == "MainPlayer")
            {
                GameObject[] enemigos = GameObject.FindGameObjectsWithTag("EnemyBerserk");
                foreach (GameObject enemigo in enemigos)
                {
                    rayDirection = enemigo.transform.position - transform.position;
                    var distanceToEnemy = Vector3.Distance(gameObject.transform.position, enemigo.transform.position);

                    if ((distanceToEnemy <= 2.5) && ((Vector3.Angle(rayDirection, transform.forward)) < areaRangeCombat))
                    {
                        objectives.Add(enemigo);
                        return true;
                    }
                }
            }

            if (gameObject.tag == "EnemyBerserk")
            {
                GameObject personaje = GameObject.FindGameObjectWithTag("MainPlayer");
                if (Vector3.Distance(gameObject.transform.position, personaje.transform.position) <= 2.5)
                {
                    objectives.Add(personaje);
                    return true;
                }
            }

            return false;
        }

        //Gestión de colisiones adicionales (flechas como proyectiles desde la torre de arquero).

        private void OnTriggerEnter(Collider other)
        {
            //Ataque de una flecha al personaje principal
            if (other.gameObject.tag == "Arrow")
            {
                if (gameObject.tag == "MainPlayer")
                {
                    Destroy(other.gameObject);

                    //tomamos el AudioSource del empty object de su hijo, no el AudioSource del MainPlayer root.
                    AudioSource audioMainPlayerDano = transform.Find("SonidoDano").gameObject.GetComponent<AudioSource>();
                    if (!audioMainPlayerDano.isPlaying)
                    {
                        audioMainPlayerDano.clip = SonidoDano;
                        audioMainPlayerDano.priority = 200;
                        audioMainPlayerDano.loop = false;
                        audioMainPlayerDano.volume = 0.2f;
                        audioMainPlayerDano.Play();
                    }
                    health -= Time.deltaTime * damagePerDeltaTime*5;
                }
            }
        }

        //Gestión del otro caso de combate mediante colisión (que tuvo que ser complementado con el de cercanía-arco para mejorar el funcionamiento).
        //Gestión también de otro tipo de colisiones "físicas".

        private void OnCollisionEnter(Collision collision)
        {
            //Ataque de un berserk al personaje principal
            if (collision.gameObject.tag=="MainPlayer" && gameObject.tag == "EnemyBerserk")
            {
                objectives.Add(collision.gameObject);
                isInAttackRange = true;
            }

            //Ataque del personaje principal a un berserk
            if (collision.gameObject.tag == "EnemyBerserk" && gameObject.tag == "MainPlayer")
            {
                objectives.Add(collision.gameObject);
                isInAttackRange = true;
            }

            //Gestión adicional para colisión con puerta en aperturas con llave.
            if (hasKey && collision.gameObject.tag == "openDoor")
            {
                propInteract = collision.gameObject.transform.parent.gameObject;
                acuTmpDoor = 0;
                hasKey = false;
                visualKey.SetActive(false);
                AudioSource.PlayClipAtPoint(doorOpen, collision.transform.position, 1f);
                Invoke("doActionProp", 0.1f);
            }

            //Gestión adicional para colisión con puerta en aperturas con llave.
            if (hasSecondKey && collision.gameObject.tag == "openFinalDoor")
            {
                propInteract = collision.gameObject.transform.parent.gameObject;
                acuTmpDoor = 0;
                hasKey = false;
                hasSecondKey = false;
                visualKey.SetActive(false);
                visualSecondKey.SetActive(false);
                AudioSource.PlayClipAtPoint(doorOpen, collision.transform.position, 1f);
                Invoke("doActionProp", 0.1f);
            }
        }

        //Perdida de fijación de enemigos en la parte de colisiones.
        private void OnCollisionExit(Collision collision)
        {
            //Ataque de un berserk al personaje principal
            if (collision.gameObject.tag == "MainPlayer" && gameObject.tag == "EnemyBerserk")
            {
                isInAttackRange = false;
            }

            //Ataque del personaje principal a un berserk
            if (collision.gameObject.tag == "EnemyBerserk" && gameObject.tag == "MainPlayer")
            {
                isInAttackRange = false;
            }
        }

        //Método adicional para apertura de puertas por el mapa.
        private void doActionProp()
        {
            acuTmpDoor++;
            if (acuTmpDoor >= 160)
            {
                return;
            }
            propInteract.transform.Rotate(new Vector3(0, -0.5f, 0));
            Invoke("doActionProp", 0f);
        }

        // ------------------------------------------------------------------------------------------//

        //Ajuste del collider tipo cápsula cuando se está agachado.
        void ScaleCapsuleForCrouching(bool crouch)
		{
			if (m_IsGrounded && crouch)
			{
				if (m_Crouching) return;
				m_Capsule.height = m_Capsule.height / 2f;
				m_Capsule.center = m_Capsule.center / 2f;
				m_Crouching = true;
			}
			else
			{
				Ray crouchRay = new Ray(m_Rigidbody.position + Vector3.up * m_Capsule.radius * k_Half, Vector3.up);
				float crouchRayLength = m_CapsuleHeight - m_Capsule.radius * k_Half;
				if (Physics.SphereCast(crouchRay, m_Capsule.radius * k_Half, crouchRayLength, Physics.AllLayers, QueryTriggerInteraction.Ignore) && gameObject.tag != "EnemyBerserk")
				{
					m_Crouching = true;
					return;
				}
				m_Capsule.height = m_CapsuleHeight;
				m_Capsule.center = m_CapsuleCenter;
				m_Crouching = false;
			}
		}

        //Evitar que el personaje se levante cuando no tiene espacio para ello en la escena.
		void PreventStandingInLowHeadroom()
		{
			if (!m_Crouching)
			{
				Ray crouchRay = new Ray(m_Rigidbody.position + Vector3.up * m_Capsule.radius * k_Half, Vector3.up);
				float crouchRayLength = m_CapsuleHeight - m_Capsule.radius * k_Half;
				if (Physics.SphereCast(crouchRay, m_Capsule.radius * k_Half, crouchRayLength, Physics.AllLayers, QueryTriggerInteraction.Ignore))
				{
					m_Crouching = true;
				}
			}
		}

        //Actualización de los valores al animador para efectuar las transiciones o mezclas de animación sobre el árbol de estados de animación.
		void UpdateAnimator(Vector3 move)
		{
			// actualizar parámetros del animador.
			m_Animator.SetFloat("Forward", m_ForwardAmount, 0.1f, Time.deltaTime);
			m_Animator.SetFloat("Turn", m_TurnAmount, 0.1f, Time.deltaTime);
            if (gameObject.tag == "EnemyBerserk")
            {
                m_Crouching = false;    //Evitar que se agache la IA en lugares indeseados.
            }
			m_Animator.SetBool("Crouch", m_Crouching);
			m_Animator.SetBool("OnGround", m_IsGrounded);
			if (!m_IsGrounded)
			{
				m_Animator.SetFloat("Jump", m_Rigidbody.velocity.y);
			}

			// Calcular que pierna está detrás para efectuar mejor la animación de salto. Sincronización.
			float runCycle =
				Mathf.Repeat(
					m_Animator.GetCurrentAnimatorStateInfo(0).normalizedTime + m_RunCycleLegOffset, 1);
			float jumpLeg = (runCycle < k_Half ? 1 : -1) * m_ForwardAmount;
			if (m_IsGrounded)
			{
				m_Animator.SetFloat("JumpLeg", jumpLeg);
			}

            //Gestión de multiplicador de velocidad de animación.
			if (m_IsGrounded && move.magnitude > 0)
			{
				m_Animator.speed = m_AnimSpeedMultiplier;
			}
			else
			{
				//No usarlo mientras se está en el aire. Efectos indeseados.
				m_Animator.speed = 1;
			}
		}

        //Gestión de movimiento en el aire. Caída, gravedad y velocidad.
		void HandleAirborneMovement()
		{
			// aplicar gravedad extra con multiplicador (desde inspector).
			Vector3 extraGravityForce = (Physics.gravity * m_GravityMultiplier) - Physics.gravity;
			m_Rigidbody.AddForce(extraGravityForce);

			m_GroundCheckDistance = m_Rigidbody.velocity.y < 0 ? m_OrigGroundCheckDistance : 0.01f;
		}

        //Gestión de movimiento en el suelo (cuando efecuar salto o agacharse).
		void HandleGroundedMovement(bool crouch, bool jump)
		{
			// comprobar condiciones válidas para cuando efectuar un salto.
			if (jump && !crouch && m_Animator.GetCurrentAnimatorStateInfo(0).IsName("Grounded"))
			{
				// saltar.
				m_Rigidbody.velocity = new Vector3(m_Rigidbody.velocity.x, m_JumpPower, m_Rigidbody.velocity.z);
				m_IsGrounded = false;
				m_Animator.applyRootMotion = false;
				m_GroundCheckDistance = 0.1f;
			}
		}

        //Ajuste de rotación de personaje para favorecer más velocidad de giro.
		void ApplyExtraTurnRotation()
		{
			float turnSpeed = Mathf.Lerp(m_StationaryTurnSpeed, m_MovingTurnSpeed, m_ForwardAmount);
			transform.Rotate(0, m_TurnAmount * turnSpeed * Time.deltaTime, 0);
		}


        //Sobreescribir método para modificar el movimiento desde root.
        //Sirve para modificar la velocidad posicional antes de que se aplique.
		public void OnAnimatorMove()
		{
			if (m_IsGrounded && Time.deltaTime > 0)
			{
				Vector3 v = (m_Animator.deltaPosition * m_MoveSpeedMultiplier) / Time.deltaTime;

				// Preservamos la componente de velocidad y actual.
				v.y = m_Rigidbody.velocity.y;
				m_Rigidbody.velocity = v;
			}
		}

        // Comprobar distancia al suelo para control de animación.
		void CheckGroundStatus()
		{
			RaycastHit hitInfo;
#if UNITY_EDITOR
			Debug.DrawLine(transform.position + (Vector3.up * 0.1f), transform.position + (Vector3.up * 0.1f) + (Vector3.down * m_GroundCheckDistance));
#endif
			if (Physics.Raycast(transform.position + (Vector3.up * 0.1f), Vector3.down, out hitInfo, m_GroundCheckDistance))
			{
				m_GroundNormal = hitInfo.normal;
				m_IsGrounded = true;
				m_Animator.applyRootMotion = true;
			}
			else
			{
				m_IsGrounded = false;
				m_GroundNormal = Vector3.up;
				m_Animator.applyRootMotion = false;
			}
		}
	}
}

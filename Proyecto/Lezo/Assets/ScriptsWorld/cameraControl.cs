﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cameraControl : MonoBehaviour
{

    public GameObject[] cameras;

    // Start is called before the first frame update
    // Inicialmente se comienza con la cámara trasera por defecto
    void Start()
    {
       cameras[0].GetComponent<Camera>().enabled = true;
       cameras[1].GetComponent<Camera>().enabled = false;

    }

    // Update is called once per frame
    void Update()
    {
  
        //Cada cambio por teclado (tecla T) hace un intercambio entre cámara principal y secundaria
        if (Input.GetButtonDown("camera"))
        {
            if (cameras[0].GetComponent<Camera>().enabled)
            {
                cameras[0].tag = "SecondCamera";
                cameras[1].tag = "MainCamera";
            } else
            {
                cameras[0].tag = "MainCamera";
                cameras[1].tag = "SecondCamera";
            }
            cameras[0].GetComponent<Camera>().enabled = !(cameras[0].GetComponent<Camera>().enabled);
            cameras[1].GetComponent<Camera>().enabled = !(cameras[1].GetComponent<Camera>().enabled);
        } 
    }
}

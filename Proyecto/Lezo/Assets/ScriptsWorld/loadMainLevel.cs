﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class loadMainLevel : MonoBehaviour
{

    public Slider sliderCharge;
    public Text textCharge;

    // Start is called before the first frame update
    void Start()
    {
        //Si estamos en la escena de carga asíncrona, invocar la carga en 5 segundos.
        if (SceneManager.GetActiveScene().name == "Carga")
        {
            Invoke("cargaNivel", 5f);
        }
        
    }

    //Co-rutina para comenzar la carga asíncrona
    void cargaNivel ()
    {
        StartCoroutine(LoadMyAsyncScene());
    }

    IEnumerator LoadMyAsyncScene()
    {
        //Evitar atascos en la carga asíncrona de segundo plano bajando su prioridad.
        Application.backgroundLoadingPriority = ThreadPriority.Low;
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync("mapaFase");
        asyncLoad.allowSceneActivation = false; //No permitir el intercambio de escenas de forma automática

        // Wait until the asynchronous scene fully loads
        float cont = 0f;
        while (!asyncLoad.isDone)
        {

            if (asyncLoad.progress >= 0.9f) //Si la escena está disponible
            {
                sliderCharge.value = asyncLoad.progress;
                textCharge.text = (asyncLoad.progress * 100).ToString("f0") + " %";
                asyncLoad.allowSceneActivation = true; //permitimos el cambio.
                sliderCharge.value = 1;
                textCharge.text = (1 * 100).ToString("f0") + " %"; //Carga al 100%
            } else
            {
                cont += 0.05f; //En otro caso, dado que el valor de progress no sigue un ajuste fino (da muy pocos valores)
                sliderCharge.value = cont; //realizamos una simulación de carga en los primeros intervalos de tiempo.
                if (cont > 0.9f)
                    cont = 0.9f;
                textCharge.text = (cont*100).ToString("f0") + " %";

            }
            yield return null;
        }

       

    }
    // Update is called once per frame
    void Update()
    {
        
    }
}

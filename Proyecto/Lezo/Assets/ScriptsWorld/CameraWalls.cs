﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraWalls : MonoBehaviour
{
    [Header("[TARGET]")]
    [SerializeField]
    private Transform target = null; //  target transform 
    [SerializeField] private string targetTag; // target tag
    [SerializeField] private string[] ignoreTag; // tags ignorados por raycast.
    [Range(0, 5.0f)]
    [SerializeField]
    private float invokeUpdateRate = 0.3f;
    [Header("[ADJUSTMENT]")]
    [SerializeField] private float distance = 4.0f;
    [SerializeField] private float height = 0.35f;
    [SerializeField] private float damping = 2.0f;
    [SerializeField] private bool smoothRotation = true;
    [SerializeField] private float rotationDamping = 3.0f;

    [SerializeField] private Vector3 targetLookAtOffset; // offsets para fijaciones de cámara muy cercanas a paredes.

    [SerializeField] private float bumperDistanceCheck = 2.5f; // bumping ray distance
    [SerializeField] private float bumperCameraHeight = 1.0f; // altura de camara en bumping
    [SerializeField] private Vector3 bumperRayOffset; // offset desde origen

    float speed = 60.0f;
    bool view_mode = false;

    //Si no se asigna el modelo para realizar seguimiento realiza su búsqueda en tiempo de ejecución
    private void Start()
    {
        if (target == null) InvokeRepeating("FindTarget", 0.5f, invokeUpdateRate);
    }

    private void FindTarget()
    {
        if (target == null)
        {
            GameObject camTarget = GameObject.FindGameObjectWithTag(targetTag);
            if (camTarget != null)
            {
                target = camTarget.transform;
            }
        }
    }

    /* Modo de cámara en rotación libre, si se mantiene pulsada la tecla ALT
     * la rotación dependerá del movimiento del ratón.
     */
    void Update()
    {
        view_mode = Input.GetKey(KeyCode.LeftAlt);

        if (view_mode)
        {
            transform.eulerAngles += new Vector3(-Input.GetAxisRaw("Mouse Y") * Time.deltaTime * speed,
                                                  Input.GetAxisRaw("Mouse X") * Time.deltaTime * speed,
                                                  0.0f);
        }

    }

    //Gestión de colocación de la cámara en tiempo de ejecución. Se pueden ignorar determinados gameObjects a este filtro.
    private void LateUpdate()
    {
        if (target != null && !view_mode)
        {
            Vector3 wantedPosition = target.TransformPoint(0, height, -distance);

            // Comprobar distancias de objetos por detrás del personaje.
            RaycastHit hit;
            Vector3 back = target.transform.TransformDirection(-1 * Vector3.forward);

            // Comprobación de si hay algo detrás
            if (Physics.Raycast(target.TransformPoint(bumperRayOffset), back, out hit, bumperDistanceCheck)
                && hit.transform != target) // ignorar los que chocan con el propio personaje.
            {
                if (ignoreTag.Length > 0)
                {
                    for (int i = 0; i < ignoreTag.Length; i++)
                    {
                        if (hit.transform.CompareTag(ignoreTag[i]))
                        {
                            return;
                        }
                    }
                }
                wantedPosition.x = hit.point.x;
                wantedPosition.z = hit.point.z;
                wantedPosition.y = Mathf.Lerp(hit.point.y + bumperCameraHeight, wantedPosition.y, Time.deltaTime * damping);
            }

            transform.position = Vector3.Lerp(transform.position, wantedPosition, Time.deltaTime * damping);

            Vector3 lookPosition = target.TransformPoint(targetLookAtOffset);

            // Ajuste de rotación suavizada.

            if (smoothRotation)
            {
                Quaternion wantedRotation = Quaternion.LookRotation(lookPosition - transform.position, target.up);
                transform.rotation = Quaternion.Slerp(transform.rotation, wantedRotation, Time.deltaTime * rotationDamping);
            }
            else
            {
                transform.rotation = Quaternion.LookRotation(lookPosition - transform.position, target.up);
            }
        }
    }
}

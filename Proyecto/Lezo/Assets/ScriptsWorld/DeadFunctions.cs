﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DeadFunctions : MenusFunctions
{
    public static bool DeadActive = false;
    public GameObject deadMenu;

    //Gestión del menu de muerte durante la partida ante el evento de muerte del protagonista.

    public void Retry()
    {
        DeadActive = false;
        Time.timeScale = 1f;
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void ChargeDead()
    {
        DeadActive = true;
        Time.timeScale = 0f;
        deadMenu.SetActive(true);
    }

    public void LoadMenu()
    {
        DeadActive = false;
        Time.timeScale = 1f;
        LoadSceneByIndex(0);        // 0 is Menu Index
    }
}

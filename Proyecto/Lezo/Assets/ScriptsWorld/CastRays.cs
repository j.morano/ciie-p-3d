﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.Characters.ThirdPerson;

public class CastRays : MonoBehaviour
{
    // Start is called before the first frame update
    private GameObject target;
    private GameObject target_m;
    private GameObject vigilante;
    private GameObject visor_v;
    private int counts;
    private AudioSource audioSourceEmptyObject;
    public AudioClip sonidoTick;
    public AudioClip sonidoAlarm;
    public AudioClip sonidoEnfado;

    //==========================================================================================
    float time = 0.0f;
    float angulo_global_x = 0.0f;
    float angulo_parcial_x = 0.0f;
    float angulo_global_y = 0.0f;
    float angulo_parcial_y = 0.0f;
    private bool objetivo_detectado = false;
    private bool en_vigilancia = true;
    float margen = 0.0f;
    Vector3 targetDir;
    float angle;

    Collider m_collider;
    private bool en_buff;
    public float speed;
    //==========================================================================================

    private GameObject[] enemiesArray;

    //Manage stealth bar
    public GameObject stealthBarUI;
    public Slider slider;

    void Start()
    {
        counts = 0;
        target = GameObject.FindGameObjectWithTag("MainPlayer"); //ThirdPerson controller
        vigilante = transform.parent.gameObject;
        audioSourceEmptyObject = target.transform.Find("SonidoTick").gameObject.GetComponent<AudioSource>();
        visor_v = GameObject.FindGameObjectWithTag("VisionVigilante");
        m_collider = visor_v.GetComponent<Collider>();
        speed = 3.5f;
    }

    private void Update()
    {
        stealthBarUI.transform.LookAt(Camera.main.transform); //mirar a cámara siempre
    }


    private void OnTriggerEnter(Collider other) {

        if (en_vigilancia)
        {
            if (other.gameObject.tag == "MainPlayer")
            {
                objetivo_detectado = true;
            }
        }

    }

    private void OnTriggerExit(Collider other)
    {
        en_vigilancia = true;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        /* Generar movimientos del haz de luz para realizar búsquedas del personaje 
         * principal. El objetivo es generar cierta sensación de aleatoriedad, pero manteniendo
         * la luz siempre cercana al personaje, siendo conocida su posición en todo momento
         */
         
        if (objetivo_detectado || en_buff) {
            vigilante.transform.LookAt(target.transform);
            vigilante.transform.Rotate(2,0,0,Space.Self);
        } else {

            time += Time.deltaTime;

            if (time > speed)
            {
                targetDir = target.transform.position - vigilante.transform.position;
                angle = -Vector3.SignedAngle(targetDir, vigilante.transform.forward, Vector3.up);

                if (vigilante.transform.rotation.eulerAngles.x -180 < 0)
                    angulo_global_x = Random.Range(-30, -10);
                else
                    angulo_global_x = Random.Range(10, 30);

                angulo_global_y = Random.Range(angle-10, angle+10);

                time = 0.0f;
            }

            angulo_parcial_x += (angulo_global_x/(speed/Time.deltaTime));
            angulo_parcial_y += (angulo_global_y/(speed/Time.deltaTime));

            vigilante.transform.localRotation = Quaternion.Euler(angulo_parcial_x, angulo_parcial_y, 0);
        }

        //==========================================================================================

        RaycastHit objectHit;
        Vector3 fwd = transform.TransformDirection(Vector3.forward);

        /* Segunda parte del algoritmo. Como viene indicado en la memoria, aquí comprobamos si
         * realmente la torre sigue viendo al protagonista o si este se ha ocultado.
         */ 

        if (objetivo_detectado)
        {
            m_collider.enabled = false;

            Debug.DrawRay(visor_v.transform.position, fwd * 60, Color.red); //70 metros de alcance max
            if (Physics.Raycast(visor_v.transform.position, fwd * 60, out objectHit, 70))
            {
                if (objectHit.transform.gameObject.tag.Equals("MainPlayer") && !en_buff)
                {
                    counts++;

                    if (counts % 25 == 0)
                    {
                        if (!stealthBarUI.activeSelf)
                            stealthBarUI.SetActive(true);

                        slider.value += 0.05f;

                        if (!audioSourceEmptyObject.isPlaying)
                        {
                            audioSourceEmptyObject.clip = sonidoTick;
                            audioSourceEmptyObject.volume = 0.7f;
                            audioSourceEmptyObject.priority = 20;
                            audioSourceEmptyObject.Play();
                        }

                    }
                    if (counts % 500 == 0 || slider.value == 1f)
                    {
                        slider.value = 1f;

                        //Penalizar jugador
                        debuffPlayer(objectHit.transform.gameObject);

                        counts = 0;
                        en_vigilancia = false;
                        objetivo_detectado = false;
                        m_collider.enabled = true;
                        en_buff = true;
                        audioSourceEmptyObject.clip = sonidoAlarm;
                        audioSourceEmptyObject.volume = 0.2f;
                        audioSourceEmptyObject.priority = 20;
                        audioSourceEmptyObject.Play();
                        Invoke("restoreFunction", 15f);
                    }
                }
                else
                {
                    decreaseCounts();
                }

            }
        }
    }

    //Decrementos de contador ante ocultación del personaje principal.

    void decreaseCounts ()
    {
        counts--;
        if (counts > 0)
        {
            if (counts % 25 == 0)
                slider.value -= 0.05f;
        }
        else
        {
            counts = 0;
            slider.value = 0;
            objetivo_detectado = false;
            en_vigilancia = false;
            m_collider.enabled = true;
            if (stealthBarUI.activeSelf)
                stealthBarUI.SetActive(false);
        }
    }

    //Penalización sobre el protagonista.
    void debuffPlayer(GameObject player)
    {
        ThirdPersonCharacter componentEnemy = player.GetComponent<ThirdPersonCharacter>();
        componentEnemy.m_MoveSpeedMultiplier /= 2;
        componentEnemy.damagePerDeltaTime /= 4;
    }

    //Restauración de estado del protagonista.
    void buffPlayer()
    {
        GameObject player = GameObject.FindGameObjectWithTag("MainPlayer");
        ThirdPersonCharacter componentEnemy = player.GetComponent<ThirdPersonCharacter>();
        componentEnemy.m_MoveSpeedMultiplier *= 2;
        componentEnemy.damagePerDeltaTime *= 4;
    }

    //Una vez finalizado el período de máxima vigilancia...
    void restoreFunction ()
    {
        audioSourceEmptyObject.clip = sonidoEnfado;
        audioSourceEmptyObject.volume = 0.2f;
        audioSourceEmptyObject.priority = 20;
        audioSourceEmptyObject.Play();

        slider.value = 0;
        if (stealthBarUI.activeSelf)
            stealthBarUI.SetActive(false);

        en_buff = false;

        //restore player state...
        buffPlayer();
    }
}

# Lezo: Una victoria, una venganza

Juego en 3D (Pr�ctica 2) de CIIE [4�] (2018/2019).

Autores:
 - David Balada Barreiro
 - ��igo Luis L�pez-Riob�o Botana
 - Jos� Morano S�nchez

### Anotaciones relevantes

Los scripts propios est�n situados en las carpetas _Assets/SriptsWorld_ y _Assets/Scripts_. Aquellos que ya exist�an y fueron adaptados a nuestras necesidades est�n en cambio en sus carpetas correspondientes, destacando aquellos que est�n en _Assets/Standard Assets/Characters/ThirdPersonCharacter/Scripts_ relacionados con los comportamientos de los personajes.

Por otra parte, el proyecto de Unity est� subido al completo, pese a que gran parte del espacio est� ocupado por la carpeta _Libraries_.
